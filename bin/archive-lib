#!/bin/bash
#
# Copyright © 2003-2007 Guillem Jover <guillem@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

#
# Requires: procmail (formail)
#

# Sanitize environment

unset LANG
unset LC_ALL

# Import configuration

. ../etc/archive.conf

# Canonicalize directories

accepted_dir=$queue_dir/accepted
rejected_dir=$queue_dir/rejected
rejected_daily_dir=$rejected_dir/`date -I`
unchecked_dir=$queue_dir/unchecked
byhand_dir=$queue_dir/byhand
buildd_dir=$accepted_dir/buildd

######
# file input functions

fetch_field ()
{
  local field=$1

  formail -z -x$field:
}

fetch_source_name ()
{
  fetch_field Source | cut -d' ' -f1
}

fetch_maintainer ()
{
  fetch_field Maintainer
}

fetch_files ()
{
  formail -xFiles: | cut -d' ' -f6
}

fetch_sha256sums ()
{
  formail -xChecksums-Sha256: | cut -d' ' -f2,4 | sed -e 's/ /  /'
}

fetch_secure_files ()
{
  local files=`fetch_files`

  for file in $files; do
    echo $(basename "`readlink -f $file`")
  done
}

fetch_single_arch ()
{
  local arches=`fetch_field "Architecture"`

  echo $(filter_real_arches $arches)
}

filter_real_arches ()
{
  local arches=$@
  local real_arches=$(echo $arches | sed -re 's/(source|all) ?//g')

  echo $real_arches
}

get_archive_arches ()
{
  local arches

  for suite in $suite_list; do
    arches="$arches $(get_suite_arches $suite)"
  done

  arches="$(echo $arches | tr ' ' '\n' | sort -u)"

  echo $(filter_real_arches $arches)
}

get_pool_dirs ()
{
  if [ "$multipool" = "yes" ]; then
    local pool_dirs

    for arch in $(get_archive_arches); do
      pool_dirs="$pool_dirs $pool_dir-$arch"
    done
    echo "$pool_dirs"
  else
    echo "$pool_dir/"
  fi
}

get_pool_dir_arch ()
{
  local arch=$1

  if [ "$multipool" = "yes" ]; then
    echo "$pool_dir-$arch/"
  else
    echo "$pool_dir/"
  fi
}

get_suite_arches ()
{
  local suite=$(canonic_suite $1)

  eval echo \$arch_list_$suite
}

get_suite_sections ()
{
  echo $section_list
}

valid_arch ()
{
  local v_suite=$1
  local v_arch=$2

  for arch in $(get_suite_arches $v_suite) all; do
    if [ "$arch" = "$v_arch" ]; then
      return 0
    fi
  done

  return 1
}

get_suite_codename()
{
  local suite=$1

  for s_alias in $suite_alias; do
    s_codename=${s_alias%%:*}
    s_suite=${s_alias##*:}

    if [ "$suite" = $s_suite ]; then
      echo $s_codename
      return 0
    fi
  done

  echo $suite
}

canonic_suite()
{
  local code_name=$1

  for s_alias in $suite_alias; do
    s_code_name=${s_alias%%:*}
    s_suite=${s_alias##*:}

    if [ "$code_name" = $s_code_name ]; then
      echo $s_suite
      return 0
    fi
  done

  echo $code_name
}

poolize_hash_name()
{
  echo $1 | sed -e 's/^\(\(lib\)\?.\).*$/\1/'
}

poolize_package_name()
{
  local name=$1
  local package=`echo $name | cut -d_ -f1`
  local section=main
  local hash_dir=`poolize_hash_name $package`
  local dest_dir=$pool_dir/$section/$hash_dir/$package/

  echo $dest_dir
}

poolize_arch_name()
{
  local package=$1
  local arch=$2
  local section=main
  local hash_dir=`poolize_hash_name $package`

  if [ "$multipool" = "yes" ]; then
    local dest_dir=$pool_dir-$arch/$section/$hash_dir/$package/
  else
    local dest_dir=$pool_dir/$section/$hash_dir/$package/
  fi

  echo $dest_dir
}

# .changes managment functions

changes_canonic ()
{
  local s_sub

  for s_alias in $suite_alias; do
    s_code_name=${s_alias%%:*}
    s_suite=${s_alias##*:}
    s_sub="sub(\"$s_code_name\", \"$s_suite\") ; $s_sub" 
  done

  while read f_changes; do
    awk "/^-----BEGIN PGP SIGNED/ { pgpstartblock = 1 }
         pgpstartblock && /^ *$/ { pgpstartblock = 0 }
         /^-----BEGIN PGP SIGNATURE/ { pgpendblock = 1 }
         /^-----END PGP SIGNATURE/ { pgpendblock = 0 }
         pgpstartblock || pgpendblock { next }
         /^Source:/ { package = \$2 }
         /^Distribution:/ { $s_sub ; suite = \$2 }
         /^Architecture:/ { sub(\"all\", \"\") ; sub(\"source\", \"\") ; arch = \$2 }
         /^Version:/ { version = \$2 }
         /^Files:/ { filesblock = 1 }
         filesblock && /^ / { files = files\" \"\$5 }
         filesblock && /^ *$/ { filesblock = 0 }
         END { print package, suite, arch, version, FILENAME files }
        " $f_changes
  done
}

changes_obsolete ()
{
  # FIXME: Should implement suite stacks for obsoleting purposes.
  #               (unstable + unreleased) < experimental
  #        Should remove the hardcoded suite checks
  while read package suite arch version path files; do
    if [ "$package" = "$g_package" ] &&
       [ "$arch" = "$g_arch" ]; then
      if dpkg --compare-versions $g_version lt $version; then
	if [ "$suite" = "unstable" -o "$g_suite" = "$suite" ]; then
          printf "%s %s %s %s %s " $g_package $g_suite $g_arch $g_version $g_path 
          echo $g_files
        fi
      else
	if [ "$g_suite" = "unstable" -o "$suite" = "$g_suite" ]; then
          printf "%s %s %s %s %s " $package $suite $arch $version $path
          echo $files
        fi
      fi
    fi
    g_package=$package
    g_version=$version
    g_suite=$suite
    g_arch=$arch
    g_path=$path
    g_files=$files
  done
}

obsolete_changes ()
{
  changes=$1
  archive=`strip_gpg < $changes`
  package=`echo "$archive" | fetch_source_name`
  version=`echo "$archive" | fetch_field "Version"`
  suite=`echo "$archive" | fetch_field "Distribution"`
  files=`echo "$archive" | fetch_files | tr '\n' ' '`

  log obsolete "  -> Obsoleting: ${package}_${version} ($suite) [$arch]"
  log obsolete "   -> Changes file: ${changes##*/}"
  log obsolete "   -> Binary files:$files"

  srcdir=$(dirname $changes)
  dstdir=$obsolete_dir/`date -I`/$pkg
  mkdir -p $dstdir
  for file in $files ; do
    mv $srcdir/$file $dstdir
  done
  mv $changes $dstdir
  rmdir --ignore-fail-on-non-empty $srcdir
}

get_archive_packages ()
{
  local arch=$1
  local suite=$2
  local ext=$3

  awk "\$2 ~ /$suite/ &&
       \$3 ~ /$arch/ {
         gsub(\"$archive_dir/\", \"\", \$5) ;
         gsub(\"[^/]*\$\", \"\", \$5) ;
         for (i = 6 ; i <= NF ; i++)
           if (\$i ~ /.*(all|$arch)\.$ext/)
             print \$5 \$i
         }
      " $cache_dir/changes_$arch.list
}

strip_gpg ()
{
  awk  '
BEGIN { block = 0; contents = 0 }
/^-----BEGIN PGP SIGNATURE/ { exit }
block && contents { print }
/^-----BEGIN PGP SIGNED/ { block = 1 }
block && /^ *$/ { contents = 1 }
'
}

archive_move ()
{
  local mode=$1
  local fetch_files='fetch_files'
  if [ $mode = "secure" ]; then
    fetch_files='fetch_secure_files'
    shift
  fi
  local archive_file=$1
  local changes_file=`basename $archive_file archive`changes
  local dest_dir=$2
  local files=`$fetch_files < $archive_file`

  chmod 600 $changes_file 2>/dev/null
  mv -f $changes_file "$dest_dir" 2>/dev/null

  if cp -f $files "$dest_dir" && rm -f $files; then
    return 0
  else
    return 1
  fi
}

files_owner_perms ()
{
  local files=$@

  chown -f $archive_owner:$archive_group $files
  chmod $archive_perms $files
}

dirs_owner_perms ()
{
  local dirs=$@

  chown -f $archive_owner:$archive_group $dirs
  chmod $archive_perms_dir $dirs
}

time_full ()
{
  date -Iseconds | tr 'T' ' '
}

echo_time ()
{
  echo "`time_full` $@"
}

logpipe()
{
  local name=$1
  local log_monthly_dir=$log_dir/`date +%Y`/`date +%m`
  local logfile=$log_monthly_dir/$name-`date -I`.log

  mkdir -p $log_monthly_dir
  dirs_owner_perms $log_monthly_dir
  tee -a $logfile
}

log()
{
  local name=$1

  shift
  echo -e "`time_full` $@" | logpipe "$name"
}

msg_queue ()
{
  local subject=$1
  local recipient=$2
  local message=`cat`

  (
    echo "$message"
    echo "$subject"
    echo
  ) | logpipe queue

  if [ "$mail_report" = "yes" ]; then
    (
      echo "From: $archive_name Archive Maintainer <$archive_maint>"
      echo "To: $recipient"
      echo "Content-Type: text/plain; charset=\"utf-8\""
      echo "Content-Transfer-Encoding: 8bit"
      echo "Subject: $subject"
      echo -n "Date: "
      date --rfc-2822
      echo ""
      echo "$message"
    ) | /usr/sbin/sendmail -t -oi -f $archive_envelope
  fi
}

