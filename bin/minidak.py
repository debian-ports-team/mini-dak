#!/usr/bin/python3
#
# Copyright (C) 2020 Aurelien Jarno <aurel32@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

import debian.deb822
import operator
import os
import re
import time

class minidak:

    config = {}


    def __init__(self, configfile):
        self.config = {}
        fd = open(configfile)
        for line in fd.readlines():
            line = re.sub('#.*', '', line)
            line = line.strip()
            if len(line) == 0:
                continue
            line = re.split('=', line, maxsplit=1)
            key = line[0].strip()
            value = line[1].strip()
            value = re.sub('(^")|("$)', '', value)
            for subst in re.findall('\$\w+', value):
                substkey = re.sub('^\$', '', subst)
                if substkey in self.config:
                    value = value.replace(subst, self.config[substkey])
            self.config[key] = value
        fd.close()


    def echo_time(self, string):
        print(time.strftime("%Y-%m-%d %H:%M:%S+0000 " + string))


    def filter_real_architectures(self, architectures):
        return filter(lambda x: x != 'all' and x != 'source', architectures)


    def get_archive_architectures(self):
        architectures = list()
        for suite in self.get_archive_suites():
            architectures.extend(self.get_suite_architectures(suite))
        architectures = sorted(list(set(architectures)))
        return self.filter_real_architectures(architectures)


    def get_archive_suites(self):
        return self.config['suite_list'].split(" ")


    def get_codename(self, value):
        for suite_alias in self.config['suite_alias'].split(" "):
            codename = suite_alias.split(":")[0]
            suite = suite_alias.split(":")[1]
            if value == suite:
                return codename
        return value


    def get_suite(self, value):
        for suite_alias in self.config['suite_alias'].split(" "):
            codename = suite_alias.split(":")[0]
            suite = suite_alias.split(":")[1]
            if value == codename:
                return suite
        return value


    def get_suite_architectures(self, suite):
        suite = self.get_suite(suite)
        if ('arch_list_' + suite) in self.config:
            return self.config['arch_list_' + suite].split(" ")
        else:
            return list()


    def get_pool_dir_architecture(self, architecture):
        if self.config['multipool'] == 'yes':
            return self.config['pool_dir'] + '-' + architecture
        else:
            return self.config['pool_dir']


    def read_changes(self):
        packages = list()
        for architecture in self.get_archive_architectures():
            directory = self.get_pool_dir_architecture(architecture)
            for root, dirnames, filenames in os.walk(directory, followlinks=False):
                for filename in filenames:
                    if not filename.endswith('.changes'):
                        continue
                    with open(os.path.join(root, filename), 'r') as fd:
                        changes = debian.deb822.Changes(fd.readlines())
                    package = {}
                    package['Architecture'] = re.sub('source|all', '', changes['Architecture']).strip()
                    package['Changes'] = filename
                    package['Directory'] = root
                    package['Files'] = list()
                    for debfile in changes['Files']:
                        package['Files'].append(debfile['name'])
                    package['Source'] = re.sub(' .*', '', changes['Source'])
                    package['Suite'] = self.get_suite(changes['Distribution'])
                    package['Version' ] = changes['Version']
                    packages.append(package)
        return packages

